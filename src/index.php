<?php

ini_set('zlib.output_compression', 'Off');

function output($data) {
  header('Content-Type: application/json');
  ob_start('ob_gzhandler');
  if (is_string($data)) {
    echo $data;
  } else {
    echo json_encode($data);
  }
  ob_end_flush();
}

function error($reason = null) {
  header('HTTP/1.0 400 Bad Request');
  $data = array('message' => 'Bad Request');
  if (is_string($reason) && !empty($reason)) {
    $data['reason'] = $reason;
  }
  output($data);
}

function download($url) {
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  $result = curl_exec($curl);
  if ($result === false) {
    $error = curl_error($curl);
    curl_close($curl);
    error($error);
    exit();
  }
  curl_close($curl);
  return $result;
}

function fetch() {
  $_GET['screen_date'] = '20170404';
  $_GET['theater_code'] = '[0101]';
  if (!isset($_GET['screen_date']) || !isset($_GET['theater_code'])) {
    return error();
  }

  $api = 'https://ticket-api.forum-movie.net/viewSchedule';

  $screen_date = $_GET['screen_date'];
  if (!preg_match('/^\d{8}$/', $screen_date)) {
    return error();
  }

  $theater_code = $_GET['theater_code'];
  if (!preg_match('/^\[\d{4}(,\d{4})*\]$/', $theater_code)) {
    return error();
  }

  $query = http_build_query(array(
    'screen_date' => $screen_date,
    'theater_code' => $theater_code,
  ));

  $url = $api . '?' . $query;

  output(download($url));
}

function main() {
  if (!isset($_SERVER['PATH_INFO'])) {
    return fetch();
  }
  return error();
}

main();
